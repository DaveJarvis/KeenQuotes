#!/usr/bin/env bash

RELEASE=$(git describe --abbrev=0 --tags)

glab release create "${RELEASE}"

cat tokens/keenquotes.pat | glab auth login --hostname gitlab.com --stdin

glab release upload "${RELEASE}" build/libs/keenquotes.jar

