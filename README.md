# KeenQuotes

KeenQuotes converts straight quotes into curly quotes.

# Demo

Try the [demo](https://whitemagicsoftware.com/keenquotes/)!

# Requirements

Download and install JRE:

* [Java 22](https://bell-sw.com/pages/downloads).

# Download

Download the application:

* [keenquotes.jar](https://gitlab.com/DaveJarvis/KeenQuotes/-/releases/permalink/latest/downloads/keenquotes.jar)

# Run

Run the software from the command-line as follows:

``` bash
java -jar keenquotes.jar < src.txt > dst.txt 2> err.txt
```

Where:

* `src.txt` -- Input document file that contains straight quotes.
* `dst.txt` -- Output document file that'll contain curled quotes.
* `err.txt` -- Error file that will note ambiguous conversion errors.

For help, run the software as follows:

``` bash
java -jar keenquotes.jar -h
```

# Software Design

The software models a lightweight natural language parser that performs a
multi-stage traversal through prose:

1. Tokenize lexemes (e.g., words, numbers, and periods).
1. Emit all quotation mark characters (single, double, primes, etc.).
1. Build an abstract syntax tree representing nested quotations.
1. Resolve as many ambiguous straight single quotes as possible.

See the source code for details.

# Apostrophes

In UNICODE, apostrophes have no semantic meaning. This unfortunate decision
means that you have a choice in how straight apostrophes are converted to
your typographic requirements. Using the `-a` option, you may provide one
of the following values:

* `regular` -- Keep the straight apostrophe (and curl using the font).
* `modifier` -- Use UNICODE character 0x02BC.
* `apos` -- Use `&apos;`, which is not HTML4-compliant.
* `aposhex` -- Use `&#x27;`, which works with HTML4.
* `quote` -- Use `&rsquo;`, which is semantically incorrect and will likely produce the desired typography.
* `quotehex` -- Use `&#8217;`, which will likely produce the desired typography without embedding the semantically misleading "name" of "right single quote".

# Build

See [BUILD.md](BUILD.md) for detailed build instructions.

