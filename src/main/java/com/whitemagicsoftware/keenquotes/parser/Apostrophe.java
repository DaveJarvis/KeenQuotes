/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
package com.whitemagicsoftware.keenquotes.parser;

/**
 * When converting quotation marks, these values are used to indicate what
 * type of entity to use in the conversion.
 */
public enum Apostrophe {
  /**
   * No conversion is performed.
   */
  CONVERT_REGULAR( "", "regular" ),
  /**
   * Apostrophes become MODIFIER LETTER APOSTROPHE ({@code &#x2bc;}).
   */
  CONVERT_MODIFIER( "&#x2bc;", "modifier" ),
  /**
   * Apostrophes become XML APOSTROPHE ({@code &apos;}).
   */
  CONVERT_APOS( "&apos;", "apos" ),
  /**
   * Apostrophes become APOSTROPHE ({@code &#x27;}).
   */
  CONVERT_APOS_HEX( "&#x27;", "aposhex" ),
  /**
   * Apostrophes become RIGHT SINGLE QUOTATION MARK ({@code &rsquo;}).
   */
  CONVERT_RSQUOTE( "&rsquo;", "quote" ),
  /**
   * Apostrophes become RIGHT SINGLE QUOTATION MARK in hex ({@code &#8217;}).
   */
  CONVERT_RSQUOTE_HEX( "&#8217;", "quotehex" );

  private final String mCode;
  private final String mType;

  Apostrophe( final String code, final String type ) {
    mCode = code;
    mType = type;
  }

  public boolean isType( final String type ) {
    return mType.equalsIgnoreCase( type );
  }

  public String toString() {
    return mCode;
  }

  /**
   * Returns the instance that matches the given type.
   *
   * @param type The type of apostrophe entity conversion to use.
   * @return The {@link Apostrophe} to use when converting entities.
   */
  public static Apostrophe fromType( final String type ) {
    for( final var apostrophe : Apostrophe.values() ) {
      if( apostrophe.isType( type ) ) {
        return apostrophe;
      }
    }

    return Apostrophe.CONVERT_REGULAR;
  }
}
